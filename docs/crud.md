# crud

`crud` is a command-line-interface (CLI) tool which wraps
around the [HTTPie] tool to perform CRUD operations on a controller
v4.  By default, it will get the symbols from the environment
variable `TESTRUN_SYMBOLS` and fall back to `SYSTEST_SYMBOLS`.

### Installation

Please install [HTTPie cli].

The plan is to publish the package to F5's internal pypi repository.
Until then, download the latest *.whl file from the dist directory
and `pip install /path/to/whl/file`

### Usages

```bash
# Enter Shell Mode, see Shell Mode section
crud

# Get help from command line
crud -h
crud --help

# Default method is GET. The following are the same:
crud /api/platform/v1/features
crud get /api/platform/v1/features
crud GET /api/platform/v1/features

# Auth: Use different credentials
crud /api/platform/v1/features --auth="user:password"
```

### Shell Mode

When typing `crud` alone, it will enter the shell mode. In this
mode, the user and issue such commands as

```bash
# Get help within the shell
help
help get
help set
help unset

# Get a resource
get /api/platform/v1/features
get /api/platform/v1/features --verbose
get /api/platform/v1/roles

# Delete a resource
delete /api/adc/v1/environments/env1
del /api/adc/v1/environments/env1
d /api/adc/v1/environments/env1

# Prints the cheat sheet
c
cheat

# These are commands to exit the shell
q
quit
exit
ctrl+D

# Set HTTPie Options
set --print=b               # No header, print just the JSON
set --verbose               # Print both the request's and response's headers
set --auth="user:password"  # Use different credentials
set                         # Shows the options

# Unset HTTPie Options
unset --print=b             # Remove this option
unset                       # Shows the options

# History
history                     # Show history
h                           # Show history
!51                         # Recalls command #51
```

### Command History

In shell mode, crud keeps track of the user's command history in
~/.config/crud-history.txt. This is an editable text file. The user
can recall previous commands using

* up/down arrows,
* ctrl+R (reverse search, similar to bash), or
* !n where n is an int that is displayed when the user show the history

### Road Map

#### Done

* Add Shell Mode
* Add the get command for both shell mode and command line
* Add the post command
* Add the delete command
* Add the put command
* Add the patch command

#### To Do


[HTTPie]: https://httpie.io/docs/cli
[HTTPie cli]: https://httpie.io/docs/cli/installation
