# cui

`cui` (Controller UI) is a command-line-interface (CLI) tool which
opens a web UI to the controller.

### Usages

`cui` does not take any command-line parameter:

```bash
cui
```

The above command will reads the symbols file specified by the
`TESTRUN_SYMBOLS` environment variable and fall back to `TESTRUN_SYMBOLS`.

`cui` then opens Chrome and browse to the controller's UI, filling
in the admin username and password.

### Bug

There is currently a bug which the web UI will end up with the
following error message:
> Something went wrong while loading the NMS platform. If the problem
> persists, please contact your system administrator.

> Failed to execute 'fetch' on 'Window': Request cannot be constructed
> from a URL that includes credentials: /modules

While waiting for the fix, the work around is to focus on the address
line of the browser (command-L), then press Return.
