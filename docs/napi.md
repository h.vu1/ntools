# napi

`napi` (NGINX API) is a command-line-interface (CLI) tool which
performs the batch CRUD operations.  By default, it will get the
symbols from the environment variable `TESTRUN_SYMBOLS` and fall
back to `SYSTEST_SYMBOLS`.


### Usages

A common usage is to create a script and pass that script's path
to `napi`:

```bash
napi myscript.txt
```

Here is  sample script, which creates an environment, a role, and
a user:

```
# Create a new environment
POST /api/adc/v1/environments
{
    "metadata": {
        "name": "env1",
        "displayName": "My Environment1",
        "description": "This is my sandbox"
    }
}

# Create a new role
POST /api/platform/v1/roles
{
    "metadata": {
        "name": "role1",
        "displayName": "Role 1",
        "description": "Full Environment Access Role"
    },
    "roleDef": {
        "permissions": [
            {
                "accessTypes": [
                    "CREATE",
                    "READ",
                    "UPDATE",
                    "DELETE"
                ],
                "feature": "ENVIRONMENT-MANAGEMENT",
                "objects": [
                    {
                        "resource": "Environments",
                        "values": [
                            "env1"
                        ]
                    }
                ]
            }
        ]
    }
}

# Create user
POST /api/platform/v1/users
{
  "metadata": {
    "name": "user1",
    "displayName": "User 1",
    "description": "First user"
  },
  "userDef": {
    "email": "user1@nginx.test",
    "firstName": "Karen",
    "lastName": "Carpenters",
    "roles": [
      {
        "ref": "/api/platform/v1/roles/role1"
      }
    ]
  }
}

# Show the newly created user
GET /api/platform/v1/users/user1
```

### Script Format

* The pound sign (#) denotes a comment line and will be ignored
* Blank lines are ignored
* The lines are divided into blocks, each blocks consists of a command,
  and optionally followed by a payload
* Commands are case insensitive, meaning "get" and "GET" are treated as
  the same.

#### The GET Command

This command reads and displays the contents of a resource.

Examples:

    GET /api/platform/v1/users/user1
    GET /api/adc/v1/environments/env1
    GET /api/platform/v1/roles/role1

#### The POST, PUT, PATCH Commands

These commands create/update resources. For examples, see the sample
script above. These commands will require a payload (JSON object)
to follow.


#### The DELETE Command

This command deletes a resource.

Examples

    DELETE /api/platform/v1/users/user1
    DELETE /api/platform/v1/roles/role1
    DELETE /api/adc/v1/environments/env1


#### The SLEEP Command

Sometimes, due to timing issue, we need to wait for a duration of
time. In that case, use the SLEEP command. The following example
will pause for 10 seconds:

    SLEEP 10
