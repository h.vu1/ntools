"""Invoke Firefox to the controller's UI."""
import time
import urllib.parse

from selenium import webdriver

from . import common


def main():
    symbols = common.Symbols()

    username = urllib.parse.quote(symbols.ctrl_admin_username)
    password = urllib.parse.quote(symbols.ctrl_admin_pass)
    url = f"https://{username}:{password}@{symbols.control_host_ips[0]}"

    options = webdriver.ChromeOptions()
    options.add_experimental_option("detach", True)
    options.add_argument("--ignore-ssl-errors=yes")
    options.add_argument("--ignore-certificate-errors")
    driver = webdriver.Chrome(options=options)
    driver.get(url)
    # BUG: Something went wrong while loading the NMS platform. If the
    # problem persists, please contact your system administrator. Failed
    # to execute 'fetch' on 'Window': Request cannot be constructed from a
    # URL that includes credentials: /modules
