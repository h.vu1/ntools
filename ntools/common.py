"""Common code which the modules share."""
import collections
import json
import os
import shutil
import subprocess


class Symbols(collections.abc.Mapping):
    """Load the symbols for use as a dict or as an object with attributes."""

    def __init__(self, path=None):
        path = path or os.getenv("TESTRUN_SYMBOLS") or os.getenv("SYSTEST_SYMBOLS")
        with open(path, "r", encoding="utf-8") as stream:
            self._data = json.load(stream)
        for name, attribute in self._data.items():
            setattr(self, name, attribute)

    def __len__(self):
        return len(self._data)

    def __iter__(self):
        return iter(self._data)

    def __getitem__(self, name):
        return self._data[name]


def json_print(obj):
    """Pretty print a JSON object."""
    if not obj:
        return
    text = json.dumps(obj, indent=4)
    if shutil.which("jq"):
        subprocess.run(["jq", "."], input=text, encoding="utf-8", check=False)
    else:
        print("jq not found")
        print(text)
