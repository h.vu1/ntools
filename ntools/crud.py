#!/usr/bin/env python3
"""
A wrapper around httpie for NGINX v4.

By default, this tool will look up either TESTRUN_SYMBOLS or
SYSTEST_SYMBOLS for symbols and determine the controller's host IP
address from that symbols file.

Examples:

    crud /api/platform/v1/certs
    crud /api/platform/v1/analytics --verbose
    crud /api/platform/v1/roles

"""
import argparse
import atexit
import cmd
import contextlib
import json
import logging
import os
import pathlib
import readline
import shlex
import subprocess
import sys
import tempfile

from . import common


logging.basicConfig(level=os.getenv("LOGLEVEL", "WARN"))
LOGGER = logging.getLogger()

DEFAULT_FLAGS = ["--verify=no"]
DEFAULT_HEADERS = ["Accept:application/json"]

USAGE = """
Examples:
    crud /api/platform/v1/features
    crud get /api/platform/v1/features
    crud /api/platform/v1/roles --verbose
    crud delete /api/adc/v1/environments/env1

See https://httpie.io/docs/cli for HTTPie flags.
See https://gitlab.com/h.vu1/ntools for more information.
""".strip()

SHELL_INTRO = """
This is a HTTPie wrapper for NGINX Controller.
Type help to get started.
Type ctrl+D, q, quit, or exit to leave the shell.
Type cheat to show the cheat sheet.
"""


def edit_payload(payload):
    """
    Invoke an editor to edit the payload.

    :param payload: A JSON object
    :return: The full path name to the temp file which contains the edited payload
    """
    with tempfile.NamedTemporaryFile(
        mode="w", encoding="utf-8", delete=False, prefix="payload-", suffix=".json"
    ) as temp_file:
        json.dump(payload, fp=temp_file, indent=True)
    atexit.register(lambda: os.unlink(temp_file.name))

    editor = os.getenv("EDITOR") or "vim"
    command = [editor, temp_file.name]
    subprocess.run(command, check=False)
    return temp_file.name


def simple_get(symbols, path):
    """Get a resource and return a JSON object."""
    control_host_ip = symbols.control_host_ips[0]
    url = f"{control_host_ip}/{path.strip('/')}"
    auth = f"{symbols.ctrl_admin_username}:{symbols.ctrl_admin_pass}"
    command = ["https", "--print=b", "--verify=no", f"--auth={auth}", "get", url]
    completed_process = subprocess.run(
        command,
        encoding="utf-8",
        capture_output=True,
        check=False,
    )
    json_object = json.loads(completed_process.stdout)
    return json_object


# pylint: disable=R0913,R0914
def perform_crud(
    symbols,
    arguments=None,
    method=None,
    flags=None,
    headers=None,
    in_shell=False,
):
    """
    Perform CRUD operation using arguments from command line.
    """
    LOGGER.debug("perform_crud(method=%r)", method)

    parser = argparse.ArgumentParser(
        description="A wrapper around HTTPie which operates on a NGINX controller",
        prog=method,
        epilog=USAGE if method is None else None,
        formatter_class=argparse.RawTextHelpFormatter,
    )
    if method is None:
        parser.add_argument("method", nargs="?", default="get", type=str.upper)
    auth = f"{symbols.ctrl_admin_username}:{symbols.ctrl_admin_pass}"
    parser.add_argument("-a", "--auth", default=auth, help="e.g. user:password")
    parser.add_argument("path")
    try:
        options, additional_flags = parser.parse_known_args(arguments)
    except SystemExit:
        if in_shell:
            return 1
        raise
    if method is None:
        method = options.method

    LOGGER.debug("options=%r", options)
    LOGGER.debug("additional_flags=%r", additional_flags)

    # Determine the payload
    has_payload = any(flag.startswith("@") for flag in additional_flags)
    if method.casefold() == "post" and not has_payload:
        payload_path = edit_payload({})
        additional_flags.append(f"@{payload_path}")
    elif method.casefold() in {"patch", "put"} and not has_payload:
        json_object = simple_get(symbols, options.path)
        payload_path = edit_payload(json_object)
        additional_flags.append(f"@{payload_path}")

    control_host_ip = symbols.control_host_ips[0]
    path = options.path.strip("/")
    url = f"{control_host_ip}/{path}"
    command = (
        ["https", f"--auth={options.auth}"]
        + (flags or DEFAULT_FLAGS)
        + additional_flags
        + [method, url]
        + (headers or DEFAULT_HEADERS)
    )

    # Put the "@/path/to/payload" at the end
    command.sort(key=lambda arg: arg.startswith("@"))

    if "--print=b" not in command:
        print(shlex.join(command))
    subprocess.run(command, check=False, encoding="utf-8")
    return 0


# pylint: enable=R0913,R0914
class ShellV4(cmd.Cmd):
    """A shell which uses HTTPie to perform CRUD operations."""

    prompt = "crud> "
    intro = SHELL_INTRO

    def __init__(self, symbols, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.symbols = symbols
        self.flags = DEFAULT_FLAGS.copy()

    def do_get(self, arguments):
        """
        Perform a GET operation.

        Examples:
            get /api/platform/v1/features
            get /api/platform/v1/features --verbose   # Show both headers
            get /api/platform/v1/features --print=b   # JSON only

        See https://httpie.io/docs/cli for more flags.
        """
        arguments = shlex.split(arguments)
        perform_crud(
            self.symbols, arguments, method="get", flags=self.flags, in_shell=True
        )

    def do_delete(self, arguments):

        """
        Perform a DELETE operation.

        Examples:

            delete /api/adc/v1/environments/env1
            delete /api/adc/v1/environments/env1 --verbose

        See https://httpie.io/docs/cli for more flags.
        """
        arguments = shlex.split(arguments)
        perform_crud(
            self.symbols, arguments, method="delete", flags=self.flags, in_shell=True
        )

    def do_patch(self, arguments):
        """
        Perform a PATCH operation.

        Examples:

            # Update a user, invoke $EDITOR to edit the payload
            patch /api/platform/v1/users/user1

            # Update using an existing payload
            patch /api/platform/v1/users/user1 @/path/to/payload.json
        """
        arguments = shlex.split(arguments)
        perform_crud(
            self.symbols, arguments, method="patch", flags=self.flags, in_shell=True
        )

    def do_post(self, arguments):
        """
        Perform a POST operation.

        Examples:

            # Create a role, crud will invoke an editor to allow editing the payload
            post /api/platform/v1/roles

            # Create an environment, using a payload
            post /api/adc/v1/environments @/tmp/new_env.json
        """
        arguments = shlex.split(arguments)
        perform_crud(
            self.symbols, arguments, method="post", flags=self.flags, in_shell=True
        )

    def do_put(self, arguments):
        """
        Perform a PUT operation.

        If there is no payload (i.e. @/path/to/payload) in the
        arguments, crud will first retrieve the JSON for the resource,
        send it to an editor to allow editing, then use it as the
        payload for the PUT operation.

        Examples:

            # EDITOR will be invoked on the payload
            put /api/adc/v1/environments/env1

            # Specify the payload
            put /api/adc/v1/environments/env1 @/path/to/payload
        """
        arguments = shlex.split(arguments)
        perform_crud(
            self.symbols, arguments, method="put", flags=self.flags, in_shell=True
        )

    # pylint: disable=C0103
    def do_EOF(self, _):
        """Exit the shell."""
        return True

    # pylint: enable=C0103

    def do_cheat(self, _):
        """c, cheat: Show cheat sheet."""
        print("q, quit, exit, or ctrl+D                 # Quit the shell")
        print("delete /api/adc/v1/environments/env1     # Delete a resource")
        print("get /api/platform/v1/roles               # Get resource")
        print("get /api/platform/v1/features            # Get different resource")
        print("get /api/platform/v1/features --print=b  # Only show JSON, no header")
        print("set                                      # Show the current flags")
        print("set --verbose                            # Turn on HTTPie verbose")
        print("unset --verbose                          # Turn off HTTPie verbose")
        print("set --print=b                            # Suppress the headers")
        print()
        print("Visit https://httpie.io/docs/cli for more information on HTTPie")
        print("Source code is at https://gitlab.com/h.vu1/ntools")

    def do_history(self, _):
        """h, history: Show shell's history."""
        count = readline.get_current_history_length()
        for index in range(1, count + 1):
            history_item = readline.get_history_item(index)
            print(f"{index:>3} {history_item}")

    def expand_history(self, line):
        """
        Handles "!n" where n is an integer
        """
        try:
            index = int(line.lstrip("!"))
        except ValueError:
            return False

        history_item = readline.get_history_item(index)
        print(history_item)
        self.onecmd(history_item)

        # Replace !N with actual history item
        history_number = readline.get_current_history_length()
        readline.replace_history_item(history_number - 1, history_item)
        return True

    def do_set(self, args):
        """
        set: Set additional HTTPie flags.

        Type c to show the cheat sheet for more information.
        """
        args = shlex.split(args)
        self.flags.extend(args)
        print("HTTPie flags:")
        print("\n".join(self.flags))
        print("\nSee: https://httpie.io/docs/cli/main-features for more flags")

    def do_unset(self, args):
        """
        Unset HTTPie flags.

        Type c to show the cheat sheet for more information.
        """
        args = shlex.split(args)
        for arg in args:
            with contextlib.suppress(ValueError):
                self.flags.remove(arg)
        print("HTTPie flags:")
        print("\n".join(self.flags))
        print("\nSee: https://httpie.io/docs/cli/main-features for more flags")

    def default(self, line):
        """
        Handle unknown command. For now, we only expand history.
        """
        line = line.strip()
        if self.expand_history(line):
            return

        print(f"Unknown syntax: {line}")

    def emptyline(self) -> bool:
        """Prevent empty line from executing the last command."""

    do_c = do_cheat
    do_d = do_delete
    do_del = do_delete
    do_exit = do_EOF
    do_h = do_history
    do_q = do_EOF
    do_quit = do_EOF


def write_unique_history(history_path):
    """Write only the unique history lines and discard least-recently-used ones."""
    readline.write_history_file(history_path)
    lines = history_path.read_text(encoding="utf-8").splitlines()
    lines.reverse()
    seen = set()
    unique_lines = []
    for line in lines:
        if line not in seen:
            unique_lines.append(line)
        seen.add(line)
    unique_lines.reverse()
    history_path.write_text("\n".join(unique_lines), encoding="utf-8")


def main():
    """Entry"""
    symbols = common.Symbols()

    if len(sys.argv) > 1:
        exit_code = perform_crud(symbols=symbols, arguments=sys.argv[1:])
        return exit_code

    # Load history
    config_dir = pathlib.Path.home() / ".config"
    config_dir.mkdir(exist_ok=True)
    history_path = config_dir / "crud-history.txt"
    with contextlib.suppress(FileNotFoundError):
        readline.read_history_file(history_path)

    try:
        shell = ShellV4(symbols)
        shell.cmdloop()
    finally:
        write_unique_history(history_path)

    return 0


if __name__ == "__main__":
    sys.exit(main())
