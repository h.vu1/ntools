#!/usr/bin/env python3
"""NGINX API script runner."""
import argparse
import base64
import contextlib
import io
import json
import os
import ssl
import time
import urllib.error
import urllib.request

from . import common


def parse_command_line():
    """Parse the script's command line options."""
    default_symbols_path = os.getenv("TESTRUN_SYMBOLS") or os.getenv("SYSTEST_SYMBOLS")

    parser = argparse.ArgumentParser()
    parser.add_argument("-s", "--symbols", default=default_symbols_path)
    parser.add_argument("api_script")
    options = parser.parse_args()
    return options


def make_api(host, username, password):
    """Create an API function."""
    root = f"https://{host}"
    ssl_context = ssl.SSLContext()
    auth = f"{username}:{password}".encode("utf-8")
    auth = base64.b64encode(auth).decode("utf-8")
    headers = {
        "Accept": "application/json",
        "Authorization": f"Basic {auth}",
        "Content-Type": "application/json",
    }

    def api(method, path, payload=None):
        """Make an API call."""
        url = f"{root}/{path.strip('/')}"
        request = urllib.request.Request(
            url, data=payload, headers=headers, method=method.upper()
        )
        try:
            response = urllib.request.urlopen(request, context=ssl_context)
            return response
        except urllib.error.HTTPError as error:
            return error

    return api


def crud(api, method, url, payload):
    """Performs CRUD."""
    print("---")
    print(f"{method} {url}")
    if payload is not None:
        common.json_print(payload)
    print()

    payload = json.dumps(payload).encode("utf-8")
    response = api(method, url, payload)
    print(f"{response.code} {response.reason}")

    if response.code != 204:
        output = json.load(response.fp)
        common.json_print(output)


def read_payload(lines):
    """Keep reading the lines until a complete payload (JSON) is found."""
    buffer = io.StringIO()
    for line in lines:
        buffer.write(line)
        with contextlib.suppress(json.JSONDecodeError):
            payload = json.loads(buffer.getvalue())
            return payload
    raise ValueError(f"Incomplete payload:\n{buffer.getvalue()}")


def main():
    """Main entry."""
    options = parse_command_line()

    symbols = common.Symbols(options.symbols)
    api = make_api(
        host=symbols.control_host_ips[0],
        username=symbols.ctrl_admin_username,
        password=symbols.ctrl_admin_pass,
    )

    with open(options.api_script, "r", encoding="utf-8") as stream:
        lines = (
            line for line in stream if line.strip() and not line.strip().startswith("#")
        )
        for line in lines:
            method, arg = line.strip().split()
            method = method.upper()
            if method == "SLEEP":
                duration = int(arg)
                time.sleep(duration)
                continue
            payload = None
            if method in {"POST", "PUT", "PATCH"}:
                payload = read_payload(lines)
            try:
                crud(api, method, arg, payload)
            except urllib.error.HTTPError as error:
                print(error)


if __name__ == "__main__":
    main()
