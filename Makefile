.PHONY: help build test black lint venv

help:
	@echo Build targets:
	@echo - build
	@echo - clean
	@echo - format
	@echo - help
	@echo - lint
	@echo - test
	@echo - venv

build:
	poetry build

clean:
	find . -name '*.pyc' -delete
	find . -name '__pycache__' -delete

format:
	black ntools/*py

lint:
	pylint ntools/*py

test:
	poetry run pytest

venv:
	poetry shell
